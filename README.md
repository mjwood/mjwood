## Mark Wood's Readme

Welcome! My name is Mark Wood! I am the GitLab product manager for [Gitaly](https://about.gitlab.com/direction/gitaly/), part of the Enablement group here at GitLab.

## Who I am

* I live in Upstate New York (about an hour south of Syracuse) on 14 acres, with my small family and large dog.
* I assist with homeschooling my son, which is a joy!
* During the summer and fall, I frequently travel with my family (and dog) in our small RV. I can often be found working from all manner of places!
* I worked for over 18 years in real-time embedded controls. Projects ranged from military engine control systems, to weapon control systems, to leading the integration of entire flight control systems for commercial aircraft.
* I have a passion for technical problems, and spent years honing my skills working on increasingly complex systems.
* I also have a passion for learning and sharing knowledge, and have taught college level courses within industry to teams around the world.

## How I operate

* I work my hours around my family, so I tend to spread out my work over a long day with breaks for home-school.
* Given that we live about 50% out of an RV in the summer and are moving around a lot, I prefer to have meetings scheduled out 48 hours in advance if possible, and I try to do the same for others.
* I don't always have internet access outside of working hours when traveling. 
* I schedule my most complex tasks when I have large breaks from meetings.
* I block off areas of my calendar to allow me to complete large or complex tasks. Please talk to me first before booking on top of these times.
* I operate off of todos and issues. I try to respond in a timely fashion, but if something is urgent, please feel free to ping me in Slack and point to the relevent issue where my assistance is needed.

## Things to know about me

* I prefer direct feedback, and will likewise provide direct feedback. If this is problematic for you, please feel free to communicate that to me so that I can try my best to communicate effectively with you.
* I don't subscribe to doing things a specific way with no reason. I like to challenge what's always been done as I feel that this leads to the most interesting conversations, and the most comprehensive solutions to problems.
* I find I get bored easily with repetitive tasks, so I seek to either remove them from workflow, or automate them where possible.
* I generally don't speak unless I feel strongly about something.
* I have received feedback that I sometimes repeat myself or go into too much detail when discussing something. I'm working on being more succinct where appropriate. Please feel free to let me know how I'm doing!

## Passions

* I firmly believe that embedded controls market could greatly benefit from DevOps practices, but are currently being overlooked. I'm in the process of exploring this opportunity, and working hard to bring some of the necessary functionality to this market through Requirements Management and Quality Management.
* I am working on growing my skills around performance indicators, and learning how to leverage them to make data informed product decisions.
